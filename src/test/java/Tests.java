import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import tree.AST;
import zimpler.Resolver;
import zimpler.io.DotLoader;
import zimpler.io.Loader;
import zimpler.io.ZimplVisitor;

class Tests {
	
	List<Loader<String>> loaders = List.of(new DotLoader());
	
	@Test
	void shortestPath() throws IOException {
		AST model = new Resolver()
				.resolveFromFilename("shortest-path.zimpl", loaders);
		String result = model.accept(new ZimplVisitor());
		System.out.println("dot-adapter/Tests/shortestPath().result:\n" + result);
		// TODO: test properly
	}
	
}
