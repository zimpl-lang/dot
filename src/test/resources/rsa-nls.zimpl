# Routing and Spectrum Allocation (RSA) Problem
# Node-link formulation involving slots (NLS)
# from https://doi.org/10.1007/s11750-018-0483-6

set V;				# network nodes
set D;				# demands
set E;				# directed arcs (links)
set L := { 1..S };	# slots

defset dminus(j):= { <i,j> in E };
defset dplus(j)	:= { <j,k> in E };

param S;			# amount of available slots per arc
param v[D];			# volume of demand <d>
param s[D];			# source of demand <d>
param t[D];			# target of demand <d>
param B;			# bandwidth carried by one slot

var y[D*E] binary;	# Equal to 1 iff arc <e> is assigned to demand <d>
var x[D*L] binary;	# Equal to 1 iff demand <d> uses slot <s> in L
var a[D*L] binary;	# Equal to 1 iff demand <d> uses slot > <s> in L
var b[D*L] binary;	# Equal to 1 iff demand <d> uses slot < <s> in L


# OBJECTIVE
#--------------------------------------
min used_links_1: sum <d> in D : sum <e> in E : y[d,e];


# RESTRICTIONS
#--------------------------------------

subto source_flow_conservation_2 : forall <d> in D :
	+ sum <e> in dminus(s(d)) : y[d,e]
	- sum <e> in dplus(s(d))  : y[e,e] == 1;

subto inner_flow_conservation_3 : forall <d> in D : 
	forall <j> in V \ { s(d), t(d) } :
		+ sum <e> in dminus(j) : y[d,e]
		- sum <e> in dplus(j)  : y[d,e] == 0;

subto slot_exclusion_4 : forall <d1,d2> in D*D | d1 != d2 : 
	forall <e> in E : forall <s> in L :
		y[d1,e] + x[d1,s] + y[d2,e] + x[d2,s] <= 3;

subto consecutive_slots_5 : forall <d> in D : forall <s> in L \ { S } :
	a[d,s] >= a[d,s+1];

subto consecutive_slots_6 : forall <d> in D : forall <s> in L \ { 1 } :
	b[d,s] >= a[d,s-1];

subto demand_satisfaction_7 : forall <d> in D, forall <s> in L :
	x[d,s] + a[d,s] + b[d,s] == 1;

subto channel_size_8 : forall <d> in D :
	B * sum <s> in L : x[d,s] >= v[d];

