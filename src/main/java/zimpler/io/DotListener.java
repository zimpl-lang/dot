package zimpler.io;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNodeImpl;

import dot.antlr.DOTBaseListener;
import dot.antlr.DOTParser.*;
import zimpler.Graph;
import zimpler.Graph.Edge;

/**
 * @author Javier MV
 * @since 0.1
 */
public class DotListener extends DOTBaseListener {
	
	Graph.Builder builder;
	Map<String, String> attrs = new LinkedHashMap<String, String>();
	
	public DotListener(String filename) {
		builder = new Graph.Builder(filename);
	}
	
	String addNode(Node_idContext ctx) {
		String id = ctx.id_().getText();
		return builder.addNode(id);
	}
	
	public Graph getResult() { return builder.getResult(); }
	
	@Override public void exitGraph(GraphContext ctx) {
		if (ctx.GRAPH() != null) {
			builder.setType(Graph.Type.UNDIRECTED);
			String msg = "Only 'digraph' is currently supported.";
			throw new RuntimeException(msg);
		} else if (ctx.DIGRAPH() != null) {
			builder.setType(Graph.Type.DIRECTED);
		} else
			throw new RuntimeException("Unexpected graph type");
		
		if (ctx.id_() != null) {
			String graphName = ctx.id_().getText();
			builder.setName(graphName);
		}
	}
	
	@Override public void exitNode_stmt(Node_stmtContext ctx) {
//		node_stmt : node_id attr_list? ;
		addNode(ctx.node_id());
		// TODO: future versions will be able to assign attributes to
		// the nodes; this is not supported yet.
		if (ctx.attr_list() != null)
			attrs.clear();
	}
	
	@Override public void exitEdge_stmt(Edge_stmtContext ctx) {
//		edge_stmt : (node_id | subgraph) edgeRHS attr_list? ;
		if (ctx.subgraph() != null || !ctx.edgeRHS().subgraph().isEmpty()) {
			String msg = "'subgraph' edge_stmt not supported at the moment.";
			throw new RuntimeException(msg);
		}
		String source = addNode(ctx.node_id());
		List<Node_idContext> ids = ctx.edgeRHS().node_id();
		if (ids.size() != 1) {
			String msg = "Only single node_id in edgeRHS is supported at the moment.";
			throw new RuntimeException(msg);
		}
		String target = addNode(ids.getFirst());
		Edge e = builder.addEdge(source, target);
//		System.err.println("attrs: " + attrs); // debug
		for (String key : attrs.keySet())
			builder.addEdgeAttr(e, key, attrs.get(key));
		attrs.clear();
	}
	
	@Override public void exitAttr_stmt(Attr_stmtContext ctx) {
//		attr_stmt : (GRAPH | NODE | EDGE) attr_list ;
		// TODO: future versions will be able to assign attributes to
		// these other elements; this is not supported yet.
		attrs.clear();
	}
	
	@Override public void exitA_list(A_listContext ctx) {
//		attr_list : ('[' a_list? ']')+ ;
//		a_list : (id_ ( '=' id_)? ','?)+ ;
		for (int i = 0; i < ctx.getChildCount(); i += 4) {
			checkValidChildrenAt(ctx.children, i);
			Id_Context lhs = (Id_Context) ctx.getChild(i+0);
			Id_Context rhs = (Id_Context) ctx.getChild(i+2);
			attrs.put(lhs.getText(), rhs.getText());
		}
	}
	
	void checkValidChildrenAt(List<ParseTree> children, int i) {
		String msg = null;
		if (children.size() < i + 2) {
			// TODO: error reporting could be improved here, showing
			// more contextual information; eg: current and missing values.
			msg = "Invalid children count. Beginning at index " +
					i + ", at least 3 elements where expected.";
		} else if (children.get(i+0).getClass() != Id_Context.class ||
			children.get(i+1).getClass() != TerminalNodeImpl.class ||
			children.get(i+2).getClass() != Id_Context.class
		) {
			// TODO: improve error reporting here!
			msg = "Invalid children at index " + i + ". " +
					"Expecting: id '=' id.";
		} else if (!children.get(i+1).getText().equals("=")) {
			msg = "Invalid token found at index " + i + ". " +
					"Expecting '=', found '" + 
					children.get(i+1).getText() + "'.";
		} else if (children.size() > i+3 &&
				(children.get(i+3).getClass() != TerminalNodeImpl.class ||
				!children.get(i+3).getText().equals(","))
		) {
			msg = "Invalid child found at index " + i + ". " + 
					"Expecting ',', found '" + 
					children.get(i+3).getText() + "'.";
		}
		
		if (msg != null)
			throw new RuntimeException(msg);
	}
	
}
