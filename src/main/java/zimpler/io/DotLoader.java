package zimpler.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import dot.DotParser;
import zimpler.Graph;
import zimpler.Resolver;
import zimpler.Graph.Edge;
import zimpler.Graph.Type;

/**
 * @author Javier MV
 * @since 0.1
 */
public class DotLoader implements Loader<String> {
	
	@Override public String acceptedExtension() { return "dot"; }
	
	@Override public String extensionDescription() {
		return "Graphviz's DOT graph format";
	}
	
	public Map<String,String> load(String filename) throws IOException {
		Graph g = loadGraph(filename);
		writeNodes(g);
		writeEdges(g);
		Map<String,String> result = adaptToProperties(g);
		return result;
	}
	
	void writeNodes(Graph g) throws IOException {
		String heading = String.format("# This is the node set of " +
				"graph %sin file %s%n", (g.name == null? "" : 
					g.name + " "), g.source);
		// TODO: it would be cool to add a comment stating |V|, similar
		// to |E| or |A| in edges csv file
		
		// from https://zimpl.zib.de/download/zimpl.pdf
//		"currently at most 65536 values are allowed in a single line".
		final int MAX_ELEMS = 65536;
		List<String> lines = new ArrayList<>();
		lines.add(heading);
		int i = 0;
		StringBuilder line = new StringBuilder();
		for (String it : g.nodes) {
			if (i < MAX_ELEMS) {
				if (i != 0)
					line.append(" ");
				line.append(it);
				++i;
			} else {
				lines.add(line.append('\n').toString());
				line = new StringBuilder();
				i = 0;
			}
		}
		if (!line.isEmpty())
			lines.add(line.append('\n').toString());
		
		TextFileWriter.write(getNodesFilename(g), lines);
	}
	
	String getNodesFilename(Graph g) {
		return String.format("graph%s_nodes.txt",
				(g.name == null? "" : "_" + g.name));
	}
	
	/** DIRECTED -> "arc", UNDIRECTED -> "edge" */
	static Map<Graph.Type, String> edgesNames = Map.of(
			Type.DIRECTED, 	"arc",
			Type.UNDIRECTED,"edge"
		);
	
	void writeEdges(Graph g) throws IOException {
		String edgeName = edgesNames.get(g.type);
		String intro = String.format("# This is the %s set of " +
				"graph %sin file %s%n", edgeName, (g.name == null? "" :
					g.name + " "), g.source);
		String attrsDesc = g.edgeAttrs.isEmpty()? "no attributes" :
			"these attributes: " + g.edgeAttrs.keySet();
		intro += String.format("# There are %d %ss and %s%n",
				g.edges.size(), edgeName, attrsDesc);
		List<String> lines = new ArrayList<>();
		lines.add(intro);
//		System.err.println("DotLoader.writeEdges().edgeAttrs: " + g.edgeAttrs); // debug
		String attrs = String.join(",", g.edgeAttrs.keySet());
		String heading = "source,target" + (attrs.isEmpty()? "" :
			"," + attrs) + "\n";
		lines.add(heading);
		for (Edge e: g.edges) {
			String values = formatEdgeAttrs(g, e.source(), e.target());
			lines.add(e.source() + "," + e.target() + values + "\n");
		}
		TextFileWriter.write(getEdgesFilename(g), lines);
	}
	
	String formatEdgeAttrs(Graph g, String source, String target) {
		if (g.edgeAttrs.isEmpty())
			return "";
		var e = new Graph.Edge(source, target);
		List<String> values = g.edgeAttrs.keySet().stream().map( attr ->
			g.edgeAttrs.get(attr).get(e)
		).toList();
		if (values.stream().anyMatch(it -> it == null)) {
			String msg = "'null' values not allowed at the moment. " + 
					String.format("Source node: %s, target node: %s, " +
							"expected attributes: %s", source, target, 
							g.edgeAttrs.keySet());
			throw new RuntimeException(msg);
		}
		return "," + String.join(",", values);
	}
	
	String getEdgesFilename(Graph g) {
		String edgeName = edgesNames.get(g.type);
		return String.format("graph%s_%ss.csv",
				(g.name == null? "" : "_" + g.name), edgeName);
	}
	
	Map<String,String> adaptToProperties(Graph g) {
		Map<String,String> result = new LinkedHashMap<>();
		// FIXME: allNumeral detection should be configurable.
		boolean allNumeral = Resolver.areAllNumeral(g.nodes);
		// from https://zimpl.zib.de/download/zimpl.pdf
//		"If all values in a file should be read into a set, this is
//		possible by use of the "<s+>" template for string values, and
//		"<n+>" for numerical values."
		String nodesDesc = Resolver.encode(getNodesFilename(g), 0, allNumeral);
		result.put("V", nodesDesc);
		if (g.name != null) {
			result.put("V" + "_" + g.name, nodesDesc);
		}
		Map<Graph.Type, String> edgesSetNames = Map.of(
				Type.DIRECTED, 	"A",
				Type.UNDIRECTED,"E"
			);
		String edgesSetName = edgesSetNames.get(g.type);
		// FIXME: doesn't call Resolver.encode() 'cause it doesn't support
		// multiple indices after ':' yet.
		String nodesType = allNumeral ? "n" : "s";
		String edgesDesc = "!" + getEdgesFilename(g) + ":" + // eg: "1s,2s" 
				"1" + nodesType + ",2" + nodesType;
		// TODO: shouldn't "E" always be defined?
		result.put(edgesSetName, edgesDesc);
		if (g.name != null) {
			// TODO: shouldn't "E" always be defined?
			result.put(edgesSetName + "_" + g.name, edgesDesc);
		}
		
		int i = 2; // accounts for "source" and "target" columns
		for (String attr : g.edgeAttrs.keySet()) {
			++i;
			Map<Graph.Edge, String> mapping = g.edgeAttrs.get(attr);
			allNumeral = Resolver.areAllNumeral(mapping.values());
			String paramId = attr + "[" + edgesSetName + "]";
			String paramDesc = Resolver.encode(getEdgesFilename(g), i, 
					allNumeral);
			result.put(paramId, paramDesc);
			if (g.name != null) {
				String namedParamId = attr + 
						"[" + g.name + "_" + edgesSetName + "]";
				result.put(namedParamId, paramDesc);
			}
		}
//		System.out.println("DotLoader.adaptToProps().props: " + result); // debug
		return result;
	}
	
	Graph loadGraph(String filename) throws IOException {
		DotParser parser = DotParser.fromFilename(filename);
		ParseTree tree = parser.graph();
		DotListener lstr = new DotListener(filename);
		ParseTreeWalker.DEFAULT.walk(lstr, tree);
		return lstr.getResult();
	}
	
}
